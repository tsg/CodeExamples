#create a correct video file:
#http://tsgdoc.socsci.ru.nl/index.php?title=Video_Codecs
#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
from psychopy import visual, core, event
 
## Setup Section
win = visual.Window([400,400], fullscr=False, monitor="testMonitor", units='cm')
 
key=event.getKeys()
while 'escape' not in key:
	key=event.waitKeys()
	if key != []:
		visual.TextStim(win, text=key).draw()   
		win.flip() 

## Closing Section
win.close()
core.quit()
