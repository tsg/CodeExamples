#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from __future__ import division

import sys
from psychopy import logging, prefs
logging.console.setLevel(logging.DEBUG)  # get messages about the sound lib as it loads
prefs.general['audioLib'] = ['sounddevice']
#prefs.general['audioDriver'] = [u'jack']
import imageio


from psychopy import sound, core, visual
imageio.plugins.ffmpeg.download()
win = visual.Window(fullscr=False, size = (600,300)) # To specify window size, add parameter [width, height]
print("create mov")
mov = visual.MovieStim(win, 'Olifanten.mov')

print("play mov")
mov.play()
core.wait(1)
mov.pause()
core.wait(1)
mov.play()
core.wait(2)
print 'done'

core.quit()
