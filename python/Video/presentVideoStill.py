#create a correct video file:
#http://tsgdoc.socsci.ru.nl/index.php?title=Video_Codecs
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from psychopy import visual 
from psychopy import core, event
import csv, random, time
 
## Setup Section
win = visual.Window([1280,720], fullscr=False, monitor="testMonitor", units='cm')
 
# append this stimulus to the list of prepared stimuli
mov = visual.MovieStim(win, 'H1.avi')
print mov.duration

while not event.getKeys(keyList=['escape']):
	mov.draw()
	win.flip()
print("before")
mov.pause()
core.wait(3.000)
mov.play()
print("after")

while mov.status != visual.FINISHED:
    mov.draw()
    win.flip()
    for key in event.getKeys():
        if key in ['space']:
            core.wait(1)
        if key in ['escape','q']:
            win.close()
            core.quit()

## Closing Section
win.close()
core.quit()
