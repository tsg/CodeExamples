#Frequency does not work deu to wait for blanking

from psychopy import visual, core, event, clock

myWin = visual.Window((600.0,600.0), waitBlanking=None)

fixSpot = visual.PatchStim(myWin,tex="none", mask="gauss", pos=(0,0), size=(0.05,0.05),color='black')
message = visual.TextStim(myWin,pos=(-0.95,-0.7),alignHoriz='left',height=0.08,text='left-drag=SF, right-drag=pos, scroll=ori')
fixSpot.setAutoDraw(True)
message.setAutoDraw(True)
myMouse = event.Mouse(win=myWin)
myMouse.setVisible(False)

while event.getKeys() == []:
	mouse_dX = [0.0,0.0,0.0]
	mouse_dY = [0.0,0.0,0.0]
	
	#set time pos(sync)
	myWin.flip()
	mouse_dX[0],mouse_dY[0] = mouse_dX[1],mouse_dY[1] = myMouse.getPos()
	#ask for new pos
	
	while (mouse_dX[0] == mouse_dX[1]) and (mouse_dY[0] == mouse_dY[1]): 
		myWin.flip()
		mouse_dX[1],mouse_dY[1] = mouse_dX[2],mouse_dY[2] = myMouse.getPos()
	time1 = clock.getTime()
	#time stamp to this new pos
	
	while (mouse_dX[1] == mouse_dX[2]) and (mouse_dY[1] == mouse_dY[2]): 
		myWin.flip()
		mouse_dX[2],mouse_dY[2] = myMouse.getPos()
	time2 = clock.getTime()
	fixSpot.setPos([mouse_dX[2], mouse_dY[2]])

	mouse1, mouse2, mouse3 = myMouse.getPressed()
	wheel_dX, wheel_dY = myMouse.getWheelRel()

	info = "frequency: %s \npressed: %s,%s,%s \nposition: %s,%s \nWheel: %s,%s" % (float(1/(time2 - time1)),mouse1, mouse2, mouse3,round(mouse_dX[2],2),round(mouse_dY[2],2),wheel_dX, wheel_dY)
	message.text = (info)
	myWin.flip()

core.quit()
