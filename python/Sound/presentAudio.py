#!/usr/bin/env python2

"""
Sound stimuli are currently an area of development in PsychoPy

Previously we used pygame. Now the pyo library is also supported.
On OSX this is an improvement (using coreaudio rather than SDL). 
On windows this should help on systems with good sound cards, 
but this is yet to be confirmed. 
See the demo hardware>testSoundLatency too

"""
from psychopy import prefs

prefs.general['audioLib'] = ['pyo']
#prefs.general['audioDriver'] = ['Primary Sound']
prefs.general['audioDriver'] = ['ASIO']
prefs.general['audioDevice'] = ['ASIO4ALL v2']
#prefs.general['audioDriver'] = [u'jack']

#import sys
#import sound first to not get an error
#from psychopy import visual
from psychopy import sound
from psychopy import core
from psychopy import event
#import serial

print("init sound")
sound.init(44100,buffer=128)
print 'Using %s(with %s) for sounds' %(sound.audioLib, sound.audioDriver)

piep = sound.Sound('bop_800.wav')

piep.play()
core.wait(1)

core.quit()

