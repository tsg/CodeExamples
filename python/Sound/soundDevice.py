#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from __future__ import division

import sys
from psychopy import logging, prefs
logging.console.setLevel(logging.DEBUG)  # get messages about the sound lib as it loads
prefs.general['audioLib'] = ['sounddevice']
#prefs.general['audioDriver'] = [u'jack']
from psychopy.constants import (PLAYING, PAUSED, FINISHED, STOPPED,
                                NOT_STARTED)


from psychopy import sound, core

print 'Using %s(with %s) for sounds' % (sound.audioLib, sound.audioDriver)

#highA = sound.Sound('A', octave=3, sampleRate=44100, secs=0.8, stereo=True)
#highA.setVolume(0.8)
#tick = sound.Sound(800, secs=0.01, sampleRate=44100, stereo=True)  # sample rate ignored because already set
#tock = sound.Sound('600', secs=0.01, sampleRate=44100, stereo=True)
'''
highA.play()
core.wait(0.8)
tick.play()
core.wait(0.4)
tock.play()
core.wait(0.6)
'''
'''ding = sound.Sound('Olifanten2.wav')
ding.play()
core.wait(ding.getDuration())
dong = sound.Sound('Olifanten2.wav')
dong.play()

core.wait(1)
'''
#tada = sound.Sound('bop_800.wav')
tada = sound.Sound('A.wav')
tada.play()
while tada.status != FINISHED:
	pass
tada.play()

core.wait(3)
print(tada.status)
print 'done'

core.quit()

# The contents of this file are in the public domain.
