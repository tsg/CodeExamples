#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from __future__ import division

import sys
from psychopy import logging, prefs
logging.console.setLevel(logging.DEBUG)  # get messages about the sound lib as it loads
prefs.general['audioLib'] = ['sounddevice']
#prefs.general['audioDriver'] = [u'jack']
import imageio


from psychopy import sound, core, visual

tada = sound.Sound('Olifanten2.wav')

tada.play()
core.wait(1)
tada.pause()
core.wait(1)
tada.play()
core.wait(4)
print 'done'

core.quit()
