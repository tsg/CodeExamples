#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import with_statement
from psychopy import core, visual, clock, event, sound
import threading

## Setup Section
win = visual.Window([600,600], fullscr=False, monitor="testMonitor")

counterSlow = 0
counterFlip = 0 

def count():
	global counterSlow
	while running:
		counterSlow +=1
		core.wait(0.1)

running = True
threading.Thread(target=count).start()

while not event.getKeys(keyList=['escape']):
	counterFlip += 1
	visual.TextStim(win, text="Slow count: \n"+str(counterSlow), pos=(-0.5, 0)).draw()
	visual.TextStim(win, text="Flip count: \n"+str(counterFlip), pos=(0.5, 0)).draw()
	win.flip()

running = False

## Closing Section
win.close()
core.quit()
