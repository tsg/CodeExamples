#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from psychopy import event, visual
import serial
 
# make a buttonbox
ser = serial.Serial("COM2", 115200, timeout = 0.10 )
win = visual.Window([200,200], fullscr=False, units="cm", monitor='myMon')

while event.getKeys() == []:
	ser.write('A1')
	ser.flush()
	x = ser.readline()
	visual.TextStim(win, text=x).draw()
 
	# black screen for 1000 ms
	win.flip()
 