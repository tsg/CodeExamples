#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from psychopy import visual, core, event
import serial

MyWin = visual.Window(fullscr= False, monitor ='testMonitor',units= 'cm',winType='pyglet')
 
# make a buttonbox
ser = serial.Serial("COM2", 115200)
Rectangle = visual.Rect(MyWin,100,100)
trialClock = core.Clock()
dump = ser.readline()

while True:
	Rectangle.setFillColor('white')
	Rectangle.setLineColor('white')
	Rectangle.draw()
	MyWin.flip()
	timeStamp=trialClock.getTime()
	tresshold = 0
	while tresshold < 1000:
		ser.write('A1')
		ser.flush()  
		dump = ser.readline()
		if dump != "":
			print dump
			tresshold = int(dump)
			print tresshold
	print trialClock.getTime() - timeStamp
	Rectangle.setFillColor('black')
	Rectangle.setLineColor('black')
	Rectangle.draw() 
	MyWin.flip()

	keys = event.waitKeys()
	print keys
	if keys[0]=='escape':
 		break
