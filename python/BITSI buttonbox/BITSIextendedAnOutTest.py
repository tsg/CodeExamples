#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from rusocsci import buttonbox
from psychopy import event, visual, core 
 
# make a buttonbox
bb = buttonbox.Buttonbox(port = 'com2')
win = visual.Window([200,200], fullscr=False, units="cm", monitor='myMon')
 
i = 0
while event.getKeys() == []:
	bb.sendMarker(val=(ord('Z')))    #select pulse time
	bb.sendMarker(val=i)           #set time of dureation pulse to 2ms
	bb.sendMarker(val=(ord('Y')))    #select marker out
	bb.sendMarker(val=0)           #set time of dureation pulse to 2ms
	bb.sendMarker(val=(ord('Y')))     #select marker out
	bb.sendMarker(val=100)           #set time of dureation pulse to 2ms
	i += 1
	print i
	if i > 255:
		i = 0
	core.wait(0.01)