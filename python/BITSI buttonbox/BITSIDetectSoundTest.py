#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from psychopy import event, visual, core
import serial
 
# make a buttonbox
ser = serial.Serial("COM2", 115200, timeout = 2 )
win = visual.Window([600,200], fullscr=False, units="cm", monitor='myMon')

x = ser.readline() #get the boot string
ser.write('CS')	

while event.getKeys() == []:
	visual.TextStim(win, text="detect sound").draw()
	win.flip()
	ser.flushInput()
	ser.write('DS')	#start detection sound onset 
	x = ser.read()
	visual.TextStim(win, text="sound detected: " + x).draw()
	win.flip()
	core.wait(0.5)
 