#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from psychopy import visual, core, event
from rusocsci import buttonbox 

MyWin = visual.Window(fullscr= False, monitor ='testMonitor',units= 'cm',winType='pyglet')
 
# make a buttonbox
bb = buttonbox.Buttonbox(port = "com2")
Rectangle = visual.Rect(MyWin,100,100)
trialClock = core.Clock()
 
while True:
	Rectangle.setFillColor('white')
	Rectangle.setLineColor('white')
	Rectangle.draw()
	MyWin.flip()
	timeStamp=trialClock.getTime()
	b = bb.waitButtons()
	print trialClock.getTime() - timeStamp
	Rectangle.setFillColor('black')
	Rectangle.setLineColor('black')
	Rectangle.draw()
	MyWin.flip()

	keys = event.waitKeys()
	print keys
	if keys[0]=='escape':
 		break
