#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from psychopy import event, visual, core
import serial
 
# make a buttonbox
ser = serial.Serial("COM2", 115200, timeout = 2 )
win = visual.Window([600,200], fullscr=False, units="cm", monitor='myMon')

x = ser.readline() #get the boot string
ser.write('CV')	#calibrate the surrounding noise 
ser.flush()

while event.getKeys() == []:
	visual.TextStim(win, text="detect voice").draw()
	win.flip()
	ser.flushInput()
	ser.write('DV')	#start detection voice onset 
	x = ser.read()
	visual.TextStim(win, text="voice detected: " + x).draw()
	win.flip()
	core.wait(0.5)
 