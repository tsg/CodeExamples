#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from rusocsci import buttonbox 
 
# make a buttonbox
bb = buttonbox.Buttonbox()
 
# wait for a single button press
b = bb.waitButtons()
 
# print the button pressed
print("b: {}".format(b))