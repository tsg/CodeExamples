#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from rusocsci import buttonbox 
 
# make a buttonbox
bb = buttonbox.Buttonbox()
 
# send a marker
bb.sendMarker(val=100)    #This is your marker code, range code 1-255