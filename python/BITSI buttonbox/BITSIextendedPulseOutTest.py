#!/usr/bin/env python
 
# import the rusocsci.buttonbox module
from rusocsci import buttonbox
from psychopy import event, visual, core 
 
# make a buttonbox
bb = buttonbox.Buttonbox()
win = visual.Window([200,200], fullscr=False, units="cm", monitor='myMon')
 
# select a function
bb.sendMarker(val=(ord('X')))    #select pulse time
bb.sendMarker(val=2)           #set time of dureation pulse to 2ms

while event.getKeys() == []:
	bb.sendMarker(val=(ord('P')))    #select marker out
	bb.sendMarker(val=1)           #set marker value 115
	core.wait(0.05)