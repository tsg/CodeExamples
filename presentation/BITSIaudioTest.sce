#create a serial:
#http://tsgdoc.socsci.ru.nl/index.php?title=ButtonBoxes
active_buttons = 0;
#button_codes = 0;

begin;

picture { 
text { caption = "every second a tone."; font_size = 16; } t_Text1; x = 0; y = 0;
} P_Text;

begin_pcl;
output_port oport = output_port_manager.get_port( 1 );

loop 
until false
begin	
	oport.send_string( "T" );

	wait_interval(1000);
end;
