#create a serial:
#http://tsgdoc.socsci.ru.nl/index.php?title=ButtonBoxes
active_buttons = 0;
#button_codes = 0;

begin;

picture { 
text { caption = " "; font_size = 16; } t_Text1; x = 0; y = 0;
text { caption = " "; font_size = 16; } t_Text2; x = 90; y = 0;
} P_Text;

begin_pcl;
output_port oport = output_port_manager.get_port( 1 );

loop 
until false
begin	
	t_Text1.set_caption("analog level [1..255]:", true);
	string sMarker = system_keyboard.get_input( P_Text, t_Text2 );	

	#oport.send_code( 89 );#select analog out 1 Y int
	oport.send_string( "Z" );#select analog out 1 Y character
	oport.send_code( int(sMarker) );
	#wait_interval(1);
	oport.send_string( "Y" );#select analog out 1 Y character
	oport.send_code( int(sMarker) );
end;
