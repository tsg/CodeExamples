begin;

text { caption = "MY TEXT"; font_size = 48; } my_text;
bitmap { filename = "WeCanSeeAll.PNG"; preload = true; } my_picture;

picture {
    plane { height = 100; width = 100; emissive = 1.0,1.0,1.0; } my_plane1;
    x = 0; y = 0; z = 0;
    plane { height = 100; width = 100; emissive = 1.0,1.0,1.0; } my_plane2;
    x = 300; y = 0; z = 0;
} my_pic;

begin_pcl;

my_plane1.set_texture( my_text.copy_to_texture() );
my_plane1.set_size( my_text.width(), my_text.height() );
my_plane2.set_texture( my_picture.copy_to_texture() );
my_plane2.set_size( my_picture.width()/2, my_picture.height()/2 );

loop
    double rot = 0.0
until
    rot > 360.0
begin
    my_pic.set_3dpart_rot( 1, 0.0, 0.0, rot );
    my_pic.set_3dpart_rot( 2, 0.0, 0.0, rot );
    my_pic.present();
    wait_interval( 20 );
    rot = rot + 1.0;
end;