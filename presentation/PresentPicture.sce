#create a correct video file:
#http://tsgdoc.socsci.ru.nl/index.php?title=Video_Codecs
active_buttons = 0;
#button_codes = 0;

begin;

picture { bitmap {filename = "WeCantSeeAll.png"; preload = true; }f_Picture; 
			 x = 0; y = 0;
} p_Picture; 

begin_pcl;

p_Picture.present();
wait_interval(1000);

f_Picture.unload();
f_Picture.set_filename("WeCanSeeAll.PNG");
f_Picture.load();

p_Picture.present();
wait_interval(1000);
