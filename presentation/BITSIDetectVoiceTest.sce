#create a serial:
#http://tsgdoc.socsci.ru.nl/index.php?title=ButtonBoxes
active_buttons = 0;
#button_codes = 0;

begin;

picture { 
text { caption = " "; font_size = 16; } t_Text1; x = 0; y = 0;
} P_Text;

begin_pcl;
output_port oport = output_port_manager.get_port( 1 );
input_port Serial = input_port_manager.get_port( 1 );

string recieveData;

loop 
until false
begin	
	Serial.clear();
	oport.send_string( "CV" );#calibrate the surrounding noise 

	oport.send_string( "DV" );#start detection voice onset 
	t_Text1.set_caption( "start detection voice onset", true );
	P_Text.present();

	loop
	until (Serial.new_code_count() > 0)
	begin 
	end;

	t_Text1.set_caption( "voice detected", true );
	P_Text.present();
	wait_interval(1000);
end;
