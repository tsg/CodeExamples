#create a correct video file:
#http://tsgdoc.socsci.ru.nl/index.php?title=Video_Codecs
active_buttons = 0;
#button_codes = 0;

begin;

picture { 
	text { caption = " "; font_size = 16; } t_Text1; x = 0; y = 0;
	text { caption = " "; font_size = 16; } t_Text2; x = 0; y = -30;
} P_Text;

begin_pcl;

output_file OutputFile = new output_file;
string sKeyboardInput;

#--------------------------Get_Kbd_Input-------------------------------------
sub GetKeyboardInput(string inpstr1) begin

	t_Text1.set_caption(inpstr1, true);
	t_Text2.set_caption(" ", true);
	P_Text.present();
	system_keyboard.set_case_mode(3); # Hoofdletters mogelijk.
	sKeyboardInput = system_keyboard.get_input( P_Text, t_Text2 ); # Hier zit de backspace ingebakken.

end;

############################ LOGFILE #######################   
#-----------------------------CreateOutput------------------------------------
sub CreateHeaderOutputFile(string str ) begin

	string sTrialData = "";
 
	sTrialData.append("header1" + "\t");   
	sTrialData.append("header2" + "\n");   

   OutputFile.open( str, false ); # don't overwrite
   OutputFile.print( sTrialData );
   OutputFile.close();

end; 

#-----------------------------WriteOutput-------------------

sub WriteTrialToOutputFile(string str ) begin

	string sTrialData = "";

	sTrialData.append("data1\t");   #write variable followed by a tab
	sTrialData.append("data2\n");   

   OutputFile.open_append ( str ); # append
   OutputFile.print( sTrialData );
   OutputFile.close();
        
end;
################################################### 

GetKeyboardInput("output file name: ");
CreateHeaderOutputFile(sKeyboardInput+ ".txt");
WriteTrialToOutputFile(sKeyboardInput+ ".txt");  

t_Text1.set_caption("output file " + sKeyboardInput + " created.", true);
P_Text.present();
wait_interval(4000);            

