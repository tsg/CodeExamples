#create a correct video file:
#http://tsgdoc.socsci.ru.nl/index.php?title=Video_Codecs
active_buttons = 0;
#button_codes = 0;

begin;

picture { 
	text { caption = " "; font_size = 16; } t_Text1; x = 0; y = 60;
	text { caption = " "; font_size = 16; } t_Text2; x = 0; y = 30;
	text { caption = " "; font_size = 16; } t_Text3; x = 0; y = 0;
	text { caption = " "; font_size = 16; } t_Text4; x = 0; y = -30;
} P_Text;

begin_pcl;

# --------------------- INPUT FILE VARIABLES INITIATION ----------------------------;
int iMaxColumns = 6;
int iTotalAmountOfTrials = 24;
array <string> asTrialInputData[iTotalAmountOfTrials][iMaxColumns];	# 2 * 12 * 6 words 
int iTrialCount;

#-----------------------------Get_stimulus_file------------------------------------
sub GetStimulusInputFile(string str ) 
begin    
	
	input_file inputfile = new input_file;
	inputfile.open( str );    
	inputfile.set_delimiter( '\n' ); 						# for get_line()
	string sDummy = inputfile.get_line();							# dummy read of the first line

	loop iTrialCount = 1 until								
		inputfile.end_of_file() || !inputfile.last_succeeded()
	begin
		sDummy = inputfile.get_line();
		sDummy.split("\t",asTrialInputData[iTrialCount]);
		iTrialCount = iTrialCount + 1;
	end;  
	
	if !inputfile.last_succeeded() then
		term.print( "Error - not enough lines in stimuli List\n" )
	end;   
	inputfile.close();
  
end;   

################################################### 

GetStimulusInputFile("inputfile.txt");

t_Text1.set_caption("this is the first word of linenr: 1", true);
t_Text2.set_caption(asTrialInputData[1][1], true);

t_Text3.set_caption("this is the last word of linenr: " + string(iTrialCount-1), true);
t_Text4.set_caption(asTrialInputData[iTrialCount-1][1], true);
P_Text.present();

wait_interval(4000);
