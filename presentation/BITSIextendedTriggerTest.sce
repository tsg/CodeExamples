#create a serial:
#http://tsgdoc.socsci.ru.nl/index.php?title=ButtonBoxes
active_buttons = 0;
#button_codes = 0;

begin;

picture { 
text { caption = " "; font_size = 16; } t_Text1; x = 0; y = 0;
text { caption = " "; font_size = 16; } t_Text2; x = 90; y = 0;
} P_Text;

begin_pcl;
output_port oport = output_port_manager.get_port( 1 );

t_Text1.set_caption("Pulse time [1..255]ms:", true);
string sTime = system_keyboard.get_input( P_Text, t_Text2 );	

#oport.send_code( 88 );#select duration X int
oport.send_string( "X" );#select duration X character
oport.send_code( int(sTime) );

loop 
until false
begin	
	t_Text1.set_caption("trigger [1..255]:", true);
	string sMarker = system_keyboard.get_input( P_Text, t_Text2 );	

	#oport.send_code( 80 );#select pulse out P int
	oport.send_string( "P" );#select pulse out P character
	oport.send_code( int(sMarker) );
end;
